export const products = [
    {
        id: 1,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 2,
        name: 'Linen-blend Shirt',
        image: '57.svg',
        price: '17',
        color: {
            color1 : '#8DB4D2',
            color2 : '#FFD1DC'
        }
    },
    {
        id: 3,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 4,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 5,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 6,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 7,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 8,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
    {
        id: 9,
        name: 'Rounded Red Hat',
        image: '49_ecfb9d65-a392-40b1-b9fb-f62e5a84f812.svg',
        price: '8',
        color: {
            color1 : '#FFD700',
            color2 : '#000'
        }
    },
];
