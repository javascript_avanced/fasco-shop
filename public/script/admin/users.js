import {$} from '../constant.js';

const users = {
    start: function() {
        this.getUsers(this.renderUsers);
    },
    getUsers(callback) {
        axios.get('http://localhost:3000/user')
            .then(response => response.data)
            .then(callback);
    },
    renderUsers(users) {
        const blockContainer = $('#container .content .table .tbody');
        const role = {
            0: 'user',
            1: 'admin',
        }
        let html = '';
        users.map((user) => {
            html += `
               <div id="row-user-${user.id}" class="row">
                <div>${user.id}</div>
                <div>${user.name}</div>
                <a class="img">${user.image !== '' ? `<img src="${user.image}" alt="" />` : '<img src="assets/images/section/default.jpg" alt="" />'}</a>
                <div>${user.email}</div>
                <div>${user.address}</div>
                <div>${role[user.role]}</div>
                <div><a onclick="form(type = 'editUsers', ${user.id})" href="#">Edit</a> / <a onclick="removeUsers(${user.id});" href="#">Remove</a></div>
               </div>
            `;
        });
        blockContainer.innerHTML = html;
    }
}

users.start();


