const handleId = () => {
    const form = document.getElementById('formAdd');
    const idInput = form.querySelector('#addIdUsers');

    axios.get('http://localhost:3000/user')
        .then(response => {
            // Lấy ID mới nhất từ dữ liệu API
            const latestId = response.data.reduce((maxId, order) => {
                return Math.max(maxId, order.id);
            }, 0);
            // Tạo ID mới bằng cách tăng ID mới nhất lên 1
            const newId = parseInt(latestId) + 1;
            idInput.value = newId.toString();
            // Sử dụng ID mới trong logic tiếp theo
            console.log('ID mới:', newId);
        })
        .catch(err => console.log('lỗi khi lấy id user mới + ' + err))
}

const addUsers = () =>  {
    const form = document.getElementById('formAdd');
    const addIdUsers = form.querySelector('#addIdUsers');
    const addNameUsers = form.querySelector('#addNameUsers');
    const addEmailUsers = form.querySelector('#addEmailUsers');
    const addPasswordUsers = form.querySelector('#addPasswordUsers');
    const addAddressUsers = form.querySelector('#addAddressUsers');
    const addImageUsers = form.querySelector('#addImageUsers');
    const addRoleUsers = form.querySelector('#addRoleUsers');

    if (addImageUsers.files.length > 0) {
        const file = addImageUsers.files[0];
        const reader = new FileReader();

        reader.onload = function(event) {
            const base64Image = event.target.result;
            console.log('Dữ liệu ảnh dạng Base64:', base64Image);
            // Gửi dữ liệu ảnh dạng Base64 đến JSON Server hoặc thực hiện các tác vụ khác

            const data = {
                "id": addIdUsers.value,
                "name": addNameUsers.value,
                "password": addPasswordUsers.value,
                "email": addEmailUsers.value,
                "address": addAddressUsers.value,
                "image": base64Image,
                "role": parseInt(addRoleUsers.value)
            }

            axios.post('http://localhost:3000/user', data)
                .then(response => {
                    console.log('Dữ liệu đã được gửi thành công:', response.data);
                    const background = document.getElementById('formEditBackground');
                    background.remove();
                    const role = {
                        0: 'user',
                        1: 'admin',
                    }
                    const blockContainer = document.querySelector('#container .content .table .tbody');
                    blockContainer.innerHTML += `
                          <div id="row-user-${data.id}" class="row">
                            <div>${data.id}</div>
                            <div>${data.name}</div>
                            <a class="img">${data.image !== '' ? `<img src="${data.image}" alt="" />` : '<img src="assets/images/section/default.jpg" alt="" />'}</a>
                            <div>${data.email}</div>
                            <div>${data.address}</div>
                            <div>${role[data.role]}</div>
                            <div><a onclick="form(type = 'editUsers')" href="#">Edit</a> / <a onclick="removeUsers(${data.id});" href="#">Remove</a></div>
                          </div>
                    `;
                })
                .catch(error => {
                    console.error('Lỗi khi gửi dữ liệu:', error);
                });

        };

        reader.readAsDataURL(file);
    } else {
        console.log('Không có tệp tin nào được chọn.');
    }
}

const removeUsers = (id) => {
    console.log(id)
    axios.delete('http://localhost:3000/user/' + id)
        .then(response => {
            console.log('Đã xóa phần tử thành công.');
            document.getElementById(`row-user-${id}`).remove();
        })
        .catch(error => {
            console.error('Đã xảy ra lỗi khi xóa phần tử:', error);
        });
}

const handleDataEdit = (id) => {
    const form = document.getElementById('formEdit');

    axios.get('http://localhost:3000/user/' + id)
        .then(response => response.data)
        .then(response => {
            console.log(response);
            form.querySelector('#editIdUsers').value = response.id;
            form.querySelector('#editNameUsers').value = response.name;
            form.querySelector('#editEmailUsers').value = response.email;
            form.querySelector('#editPasswordUsers').value = response.password;
            form.querySelector('#editAddressUsers').value = response.address;
            form.querySelector('#editRoleUsers').value = response.role;
        });
}

const editUsers = (id) => {
    const form = document.getElementById('formEdit');
    const editNameUsers = form.querySelector('#editNameUsers');
    const editEmailUsers = form.querySelector('#editEmailUsers');
    const editPasswordUsers = form.querySelector('#editPasswordUsers');
    const editAddressUsers = form.querySelector('#editAddressUsers');
    const editImageUsers = form.querySelector('#editImageUsers');
    const editRoleUsers = form.querySelector('#editRoleUsers');

    if (editImageUsers.files.length > 0) {
        const file = editImageUsers.files[0];
        const reader = new FileReader();

        reader.onload = function(event) {
            const base64Image = event.target.result;

            const data = {
                "name": editNameUsers.value,
                "password": editPasswordUsers.value,
                "email": editEmailUsers.value,
                "address": editAddressUsers.value,
                "image": base64Image,
                "role": parseInt(editRoleUsers.value)
            }

            axios.put('http://localhost:3000/user/' + id, data)
                .then(response => {
                    console.log('Dữ liệu đã được gửi thành công:', response.data);
                    const background = document.getElementById('formEditBackground');
                    background.remove();
                    const role = {
                        0: 'user',
                        1: 'admin',
                    };
                    document.getElementById(`row-user-${id}`).innerHTML = `
                        <div>${id}</div>
                        <div>${data.name}</div>
                        <a class="img">${data.image !== '' ? `<img src="${data.image}" alt="" />` : '<img src="assets/images/section/default.jpg" alt="" />'}</a>
                        <div>${data.email}</div>
                        <div>${data.address}</div>
                        <div>${role[data.role]}</div>
                        <div><a onclick="form(type = 'editUsers', ${id})" href="#">Edit</a> / <a onclick="removeUsers(${id});" href="#">Remove</a></div>
                    `;
                })
                .catch(error => {
                    console.error('Lỗi khi gửi dữ liệu:', error);
                });

        };

        reader.readAsDataURL(file);
    } else {
        const data = {
            "name": editNameUsers.value,
            "password": editPasswordUsers.value,
            "email": editEmailUsers.value,
            "address": editAddressUsers.value,
            "role": parseInt(editRoleUsers.value)
        }

        data.image = '';

        axios.put('http://localhost:3000/user/' + id, data)
            .then(response => {
                const background = document.getElementById('formEditBackground');
                background.remove();
                const role = {
                    0: 'user',
                    1: 'admin',
                };
                document.getElementById(`row-user-${id}`).innerHTML = `
                    <div>${id}</div>
                        <div>${data.name}</div>
                        <a class="img">${data.image !== '' ? `<img src="${data.image}" alt="" />` : '<img src="assets/images/section/default.jpg" alt="" />'}</a>
                        <div>${data.email}</div>
                        <div>${data.address}</div>
                        <div>${role[data.role]}</div>
                    <div><a onclick="form(type = 'editUsers', ${id})" href="#">Edit</a> / <a onclick="removeUsers(${id});" href="#">Remove</a></div>
                    `;
            })
            .catch(error => {
                console.error('Lỗi khi gửi dữ liệu:', error);
            });
    }

}
