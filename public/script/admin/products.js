import {$, $$} from '../constant.js';

const products = {
    category: [],
    start: function() {
        const getCategoryPromise = new Promise((resolve) => {
            this.getCategory((categories) => {
                categories.map((category => {
                    this.category.push(category);
                }));
                resolve();
            });
        });
        const getProductsPromise = new Promise((resolve) => {
            this.getProducts((products) => {
                this.renderListProducts(products, this.category);
                resolve();
            });
        });

        Promise.all([getCategoryPromise, getProductsPromise]).then(() => {
            console.log(this.category);
        });
    },
    getProducts(callback) {
        axios.get('http://localhost:3000/products')
            .then(data => data.data)
            .then(callback);
    },
    getCategory(callback) {
        axios.get('http://localhost:3000/categories')
            .then(data => data.data)
            .then(callback);
    },
    renderListProducts(products, category) {
        console.log(category);
        const listBoxProduct = $('#container .content .table .tbody');
        let html = '';
        products.map(product => {
            const matchedCategory = category.find(cat => cat.id === product.id_category);
            const categoryName = matchedCategory ? matchedCategory.name : 'Unknown Category';

            html += `
              <div class="row">
                <div>${product.id}</div>
                <div>${product.name}</div>
                <div class="img"><img src="assets/images/products/${product.image}" alt=""></div>
                <div>${categoryName}</div>
                <div>${product.designer}</div>
                <div>${product.price}$</div>
                <div>
                  <span style="background: ${product.color.color1}"></span>
                  <span style="background: ${product.color.color2}"></span> 
                </div>
                <div><a onclick="form(type = 'editProduct')" href="#">Edit</a> / <a>Remove</a></div>
              </div>
            `;
        });
        listBoxProduct.innerHTML += html;
    }
}

products.start();


