const getUsers = (callback) => {
    axios.get('http://localhost:3000/user')
        .then((response) => response.data)
        .then(callback)
}

const logIn = () => {
    const signInEmail = document.getElementById('SignInEmail').value;
    const signInPassword = document.getElementById('SignInPassword').value;

    if(signInEmail && signInPassword) {
        getUsers((data) => {
            const user = data.find(user => user.email === signInEmail && user.password === signInPassword);
            if (user) {
                console.log('Đăng nhập thành công');
                localStorage.setItem('user', JSON.stringify(user));
                if(user.role === 1) {
                    window.location.href = 'admin.html';;
                }
            } else {
                console.log('Tên người dùng hoặc mật khẩu không chính xác');
            }
        });
    }
}