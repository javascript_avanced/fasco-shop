import { $ } from '../constant.js';
import { $$ } from '../constant.js';

const detail = {
    start: function() {
        const urlCurrent = window.location.href;
        const url = new URL(urlCurrent);
        const id = url.searchParams.get("id");
        this.getDetailProduct(id,this.renderDetail);
        this.handleQuantity();
    },
    getDetailProduct(id, callback) {
        axios.get(`http://localhost:3000/products/${id}`)
            .then((data) => data.data)
            .then(callback);
    },
    renderDetail(product) {
        const mainImage = $('.container .content .section1 .gallery .main_gallery img');
        mainImage.src = `assets/images/products/${product.image}`;
        $('.container .content .section1 .information .name h1').textContent = product.name;
        $('.container .content .section1 .information .price .price_now').textContent = product.price > 10 || product.price % 1 !== 0 ? `${product.price}$` : `${product.price}.00` ;
        $('.container .content .section1 .information .price .sale_price').textContent = '$' + (Math.floor(product.price * 1.33)) + '.00';
        const boxImages = $('.container .content .section1 .gallery .gallery_bar');
        let imageStr = '';
        imageStr += `
            <div>
                <img class="full" src="assets/images/products/${product.image}" alt="">
            </div>
        `;
        product.images?.map((image) => {
            imageStr += `
              <div>
                <img class="full" src="assets/images/products/${image.name}" alt="">
              </div>
            `;
        });
        boxImages.innerHTML = imageStr;
        const images = $$('.container .content .section1 .gallery .gallery_bar div img');
        images.forEach((img) => {
            img.addEventListener('click', (e) => {
                mainImage.style = 'opacity: 0.5';
                setTimeout(() => {
                    mainImage.src = e.target.src;
                    mainImage.style = 'opacity: 1';
                }, 500);
            })
        });
    },
    handleQuantity() {
        const countDown = $('.information .quantity .bottom .count_product .count_down');
        const countUp = $('.information .quantity .bottom .count_product .count_up');
        const number = $('.information .quantity .bottom .count_product div');
        countDown.addEventListener('click', (e) => {
            e.preventDefault();
            let newNumber = parseInt(number.textContent);
            if(newNumber > 1) newNumber -= 1;
            number.textContent = newNumber;
        });
        countUp.addEventListener('click', (e) => {
            e.preventDefault();
            let newNumber = parseInt(number.textContent);
            if(newNumber < 10) newNumber += 1;
            number.textContent = newNumber;
        })
    }
}

detail.start();