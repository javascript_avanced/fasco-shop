import { $ } from '../constant.js';

const shop = {
    start: function() {
        // this.renderBoxProduct(products);
        this.getProducts(this.renderBoxProduct);
    },
    getProducts(callback, idCategories = 2) {
        const products = axios.get('http://localhost:3000/products');
        products
            .then((category) => category.data)
            .then((category) => {
                console.log(category);
                let categories = []
                category.map((category, index) => {
                    if(index <= 8) {
                        if(parseInt(category.id_category) === idCategories) {
                            categories.push(category);
                        }
                    }
                });
                return categories;
            })
            .then(callback);
    },
    renderBoxProduct: function (products) {
        const container = $('.section1 .box_product .main_box_product .main_content');
        let html = '';
        products.map((product) => {
            html += `
                <div class="product_box">
                    <a href="detail.html?id=${product.id}" class="img">
                        <img class="full" src="assets/images/products/${product.image}" alt="">
                    </a>
                    <h1 onclick="handleClickProduct(${product.id})" class="name">
                        <a href="detail.html">${product.name}</a>
                    </h1>
                    <div class="price">
                        $${product.price}
                    </div>
                    <div class="tag flex">
                        <div style="background: ${product.color.color1}" class=""></div>
                        <div style="background: ${product.color.color2}" class=""></div>
                    </div>
                </div>
            `;
            container.innerHTML = html;
        })
    },
};

shop.start();
