'use strict';
import {$} from './constant.js';

const app = {
    start: function() {
        console.log('start');
        this.getCategory(this.renderCategory);
        this.getProducts(this.renderBoxProduct);
    },
    getCategory(callback) {
        const category = axios.get('http://localhost:3000/categories');
        category
            .then((category) => category.data)
            .then(callback);
    },
    getProducts(callback, idCategories = 2) {
        const products = axios.get('http://localhost:3000/products');
        products
            .then((category) => category.data)
            .then((category) => {
                console.log(category);
                let categories = []
                category.map((category, index) => {
                    if(index <= 5) {
                        if(parseInt(category.id_category) === idCategories) {
                            categories.push(category);
                        }
                    }
                });
                return categories;
            })
            .then(callback);
    },
    renderCategory(category) {
        const blockCategories = $('.container .content .section4 .section4_nav');
        let html = '';
        category.map((category) => {
            html += `<a ${parseInt(category.id) === 2 ? 'class="active"' : ''} href="#">${category.name}</a>`;
        });
        blockCategories.innerHTML = html;
    },
    renderBoxProduct(products) {
        const blockProduct = $('.container .content .section4 .box-product');
        let html = '';
        products.map((product) => {
            html += `
                  <div class="product">
                    <a href="detail.html?id=${product.id}">
                        <img src="assets/images/products/${product.image}" alt="">
                    </a>
                    <div class="flex top_content">
                        <div class="name">
                            ${product.name}
                        </div>
                        <div class="star">
                            <svg xmlns="http://www.w3.org/2000/svg" width="95" height="19" viewBox="0 0 95 19" fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M11.6646 7.12771L9.5 0L7.33536 7.12771H0L5.93479 11.742L3.73214 19L9.5 14.5146L15.2679 19L13.0652 11.742L19 7.12771H11.6646Z" fill="#FCA120"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M30.6646 7.12771L28.5 0L26.3354 7.12771H19L24.9348 11.742L22.7321 19L28.5 14.5146L34.2679 19L32.0652 11.742L38 7.12771H30.6646Z" fill="#FCA120"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M49.6646 7.12771L47.5 0L45.3354 7.12771H38L43.9348 11.742L41.7321 19L47.5 14.5146L53.2679 19L51.0652 11.742L57 7.12771H49.6646Z" fill="#FCA120"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M68.6646 7.12771L66.5 0L64.3354 7.12771H57L62.9348 11.742L60.7321 19L66.5 14.5146L72.2679 19L70.0652 11.742L76 7.12771H68.6646Z" fill="#FCA120"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M87.6646 7.12771L85.5 0L83.3354 7.12771H76L81.9348 11.742L79.7321 19L85.5 14.5146L91.2679 19L89.0652 11.742L95 7.12771H87.6646Z" fill="#FCA120"/>
                            </svg>
                        </div>
                    </div>
                    <div class="author">
                        ${product.designer}
                    </div>
                    <div class="view">
                        (${(product.review / 10000)}k) Customer Reviews
                    </div>
                    <div class="flex bottom_content">
                        <div class="price">
                            $${product.price}0
                        </div>
                        <div class="status">
                            Almost Sold Out
                        </div>
                    </div>
                </div>
            `;
        });
        blockProduct.innerHTML = html;
        console.log(products);
    }
};

app.start();